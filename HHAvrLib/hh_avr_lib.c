/*
 * hh_avr_lib.c
 *
 *  Created on: Nov 23, 2013
 *      Author: blacksun
 *
 */

#include "hh_avr_lib.h"

#include <avr/io.h>
#include <util/delay.h>


/*************************************************************************************************/
/*                                      RS232                                                    */
/*************************************************************************************************/

void hhavr_rs232_send_char(const unsigned char data)
{
	while(!(UCSRA & (1<<UDRE)));
	UDR = data;
}

void hhavr_rs232_connect(const long unsigned int fosc, const unsigned int baudrate)
{
	unsigned int ubrr = ((fosc/16/baudrate)-1);

	UBRRH = (0 << URSEL) | (ubrr >> 8);
	UBRRL = ubrr;

	/* No double transmission speed, no multi processor com mode */
	UCSRA = (0 << U2X) | (0 << MPCM);

	/* No interrupts, Enable receiver and transmitter, no 9 bit transmissions */
	UCSRB = (0 << RXCIE) | (0 << TXCIE) | (0 << UDRIE) | (1 << RXEN) | (1 << TXEN) | (0 << UCSZ2) | (0 << RXB8) | (0 << TXB8);

	/* Select UCSRC buffer: Async, Frame format: 8O1 */
	UCSRC = (1 << URSEL) | (0 << UMSEL) | (1 << UPM1) | (1 << UPM0) | (0 <<USBS) | (1 << UCSZ1) | (1 << UCSZ0);
}

void hhavr_rs232_disconnect()
{

}

void hhavr_rs232_send(const char *data, int len)
{
	for (int i = 0; i < len; i++) {
		hhavr_rs232_send_char(data[i]);
	}
}

void hhavr_rs232_recv(char *buf, const int bytesToRead)
{
	for (int i = 0; i < bytesToRead; i++) {

		if (!(UCSRA & (1<<RXC)));
		buf[i++] = UDR;
	}
}

/*************************************************************************************************/
/*                                        I/O                                                    */
/*************************************************************************************************/

// Interrupt on rising edge of PIND.2
void hhavr_int0_init()
{
	DDRB = ((0 << PIN2) | (1 << PIN1) | (0 << PIN0));
	PORTD |= (1 << PIN2);

	// Enable external pin interrupt 0
	GICR |= (1 << INT0);

	// Enable interrupt on rising flank
	MCUCR = (1 << ISC00) | (1 << ISC01);

	// Clear all interrupts
	GIFR |= (1 << INT0);
}

void hhavr_int0_clearInts()
{
	GIFR |= (1 << INT0);
}

int hhavr_io_port_output_init(hhavr_port port)
{
	switch (port) {
		case HHAVR_PORT_A:
			DDRA=0xFF;
			break;
		case HHAVR_PORT_B:
			DDRB=0xFF;
			break;
		case HHAVR_PORT_C:
			DDRC=0xFF;
			break;
		case HHAVR_PORT_D:
			DDRD=0xFF;
			break;
		default:
			break;
	}
	return HHAVR_ERROR_NO_DATA;
}
