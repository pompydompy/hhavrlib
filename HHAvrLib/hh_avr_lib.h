/*
 * hh_avr_lib.h
 *
 *  Created on: Nov 23, 2013
 *      Author: blacksun
 *
 *  Library to access default device functionality of the AVR devices
 *  Currently supported: ATMEL_AVR_MEGA_8535
 */

#ifndef HH_AVR_LIB_H_
#define HH_AVR_LIB_H_


typedef enum {
	HHAVR_PORT_A=0,
	HHAVR_PORT_B,
	HHAVR_PORT_C,
	HHAVR_PORT_D,
} hhavr_port;

typedef enum {
	HHAVR_ERROR_NONE = 0,
	HHAVR_ERROR_NO_DATA = -1, 		//!< No data available while requested for it
	HHAVR_ERROR_FRAME = -2,			//!< Frame error
	HHAVR_ERROR_DATA_OVERRUN = -3,	//!< Data overrun error
	HHAVR_ERROR_PARITY_ERROR = -4,  //!< Parity error

} hhavr_error;
/**
 * Connect via rs232
 * baudrate   The baudrate to connect with
 */
void hhavr_rs232_connect(const long unsigned int fosc, const unsigned int baudrate);

/**
 * Disconnect rs232
 */
void hhavr_rs232_disconnect(void);

/**
 * Send data to rs232
 * data     Buffer containing data to send
 * len		Number of bytes to send
 */

void hhavr_rs232_send(const char *data, int len);

/**
 * Receive data from rs232
 * buf      Buffer to store the received data in
 * bufSiz   Buffer size (maximum read amount of data)
 */
void hhavr_rs232_recv(char *buf, const int bufSize);

/**
 * Initialize PIN D2 interrupt (INT0) on rising edge
 */
void hhavr_int0_init();

/**
 * Clear all interrupts for INT0
 */
void hhavr_int0_clearInts();

/**
 * Initialize a complete port as output
 * port     The port to initialize
 * return   HHAVR_ERROR_NONE
 */
int hhavr_io_port_output_init(hhavr_port port);


#endif /* HH_AVR_LIB_H_ */
